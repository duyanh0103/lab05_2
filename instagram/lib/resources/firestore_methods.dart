import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:instagram/models/post.dart';
import 'package:instagram/resources/storage_methods.dart';
import 'package:uuid/uuid.dart';

class FirestoreMethods {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  // upload post
  Future<String> uploadPost(
    String description,
    Uint8List file,
    String uid,
    String username,
    String profImage,
  ) async {
    String res = "some error occured";
    try {
      String photoUrl =
          // cái này sẽ giúp upload lên storage
          await StorageMethods().uploadImageToStorage('posts', file, true);
      //cái này thì cho cái id unique để khỏi lo bị trùng
      String postId = const Uuid().v1();

      Post post = Post(
          description: description,
          uid: uid,
          username: username,
          postId: postId, //có thể 1 user đăng nhiều post
          datePulished: DateTime.now(),
          postUrl: photoUrl,
          profImage: profImage,
          likes: []);
      // giờ upload lên firebase
      // vì nó sẽ là collection of posts trong firebase mỗi user
      //đều có id thì mỗi cái post đều có id cho mỗi thg user đó
      _firestore.collection('posts').doc(postId).set(
            post.toJson(),
          );
      res = "success";
    } catch (err) {
      res = err.toString();
    }
    return res;
  }
}
