import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class StorageMethods {
  final FirebaseStorage _storage = FirebaseStorage.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // add a function to add image to firebase storage
  Future<String> uploadImageToStorage(
      String childName, Uint8List file, bool isPost) async {
    // cai ref tren nay null vi ko file nao ca
    // de child có thể là 1 folder và folder đó có thể tồn tại hoặc ch tồn tại
    // firebase sẽ giúp nếu chưa có thì tạo nếu có r thì dzo
    // để get userId khởi tạo firebaseAuth
    Reference ref =
        _storage.ref().child(childName).child(_auth.currentUser!.uid);

    if (isPost) {
      String id = const Uuid().v1();
      // the name of the post will be that unique id
      ref = ref.child(id);
    }

    // upload the task
    // take ref and mỗi khi mà folder structure mà chúng ta đã tạo
    // we muốn lấy folder đó và bỏ file vào location đó
    UploadTask uploadTask = ref.putData(file);

    TaskSnapshot snap = await uploadTask;

    String downloadUrl = await snap.ref.getDownloadURL();

    return downloadUrl;
  }
}
